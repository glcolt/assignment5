def get_first_available_id(books):
    books.sort(key=lambda book : int(book['id']))
    for i, j in zip([int(book['id']) for book in books], range(1, len(books) + 1)):
        if i != j:
            return j
    return len(books) + 1
