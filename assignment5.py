#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

from utils import get_first_available_id

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON/')
def bookJSON():
    return json.dumps(books)

@app.route('/')
@app.route('/book/')
def showBooks():
    return render_template('showBooks.html', books=books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        id = get_first_available_id(books)
        books.append({'title': request.form['title'], 'id': str(id)})
        return redirect('/')
    else:
        return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    book = next((book for book in books if book['id'] == str(book_id)), None)
    if book is None:
        return render_template('showBooks.html', books=books)
    elif request.method == 'POST':
        book['title'] = request.form['title']
        return redirect('/')
    else:
        return render_template('editBook.html', book=book)
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    book = next((book for book in books if book['id'] == str(book_id)), None)
    if book is None:
        return render_template('showBooks.html', books=books)
    elif request.method == 'POST':
        books.remove(book)
        return redirect('/')
    else:
        return render_template('deleteBook.html', book=book)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

